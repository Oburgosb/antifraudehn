﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TSEAntifraudeHn
{
    public class ActasRepository : IActasRepository
    {

        private readonly TSEAntifraudeHnContext _context = null;

        public ActasRepository(IOptions<Settings> settings)
        {
            _context = new TSEAntifraudeHnContext(settings);
        }

        public async Task<bool> EliminarTodo()
        {
            try
            {
                DeleteResult actionResult
                    = await _context.Actas.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public void EliminarActas(List<Acta> actas)
        {
            try
            {
                var actasId = actas.Select(x => x.CodActa).ToList();
                _context.Actas.DeleteMany(_ => actasId.Contains(_.CodActa));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public int Count(string departamento)
        {
            return (int)_context.Actas.Find(_ => _.NomDepartamento == departamento).Count();
        }

        public async Task<Acta> ObtenerActa(string codigoActa)
        {
            var filter = Builders<Acta>.Filter.Eq("CodActa", codigoActa);

            try
            {
                return await _context.Actas
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<IEnumerable<Acta>> ObtenerTodas()
        {
            try
            {
                return await _context.Actas.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<IEnumerable<Acta>> ObtenerPorDepartamento(string departamento, int skip, int take)
        {
            try
            {
                return await _context.Actas.Find(_ => _.NomDepartamento == departamento).Skip(skip).Limit(take).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task RegistrarActa(Acta item)
        {
            try
            {
                await _context.Actas.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task RegistrarActas(IEnumerable<Acta> items)
        {
            try
            {
                await _context.Actas.InsertManyAsync(items);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task RegistrarActasEnLog(IEnumerable<Acta> items)
        {
            try
            {
                var actasSnapshot = new ActaSnapshot
                {
                    FechaCopia = DateTime.Now,
                    Actas = items.ToList()
                };
                await _context.ActasLog.InsertOneAsync(actasSnapshot);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}
