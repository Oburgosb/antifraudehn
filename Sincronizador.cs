﻿using System;
using System.Collections.Generic;
using Helpers.Web;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TSEAntifraudeHn
{
    [Route("api/Sincronizador")]
    public class Sincronizador : Controller
    {
        private readonly IActasRepository _actasRepository;

        public Sincronizador(IActasRepository noteRepository)
        {
            _actasRepository = noteRepository;
        }

        [Route("Sincronizar")]
        public IActionResult Sincronizar()
        {
            SincronizarActas();
            return Ok();
        }

        [Route("Iniciar/{inicio}/{final}")]
        public async void RecargarActas(int inicio = 1, int final = 10)
        {
            try
            {
                var actasNuevas = new List<Acta>();
                for (int i = inicio; i <= final; i++)
                {
                    var cliente = new HttpClient2(@"https://api.tse.hn/prod/ApiActa/Consultar/1");

                    var acta = await cliente.GetAsync<Acta>(i.ToString());
                    if (acta.Ok)
                    {
                        actasNuevas.Add(acta.Data);
                    }
                }

                //var actasActuales = await _actasRepository.ObtenerTodas();
                // await _actasRepository.RegistrarActasEnLog(actasActuales);
                // await _actasRepository.EliminarTodo();

                _actasRepository.EliminarActas(actasNuevas);
                await _actasRepository.RegistrarActas(actasNuevas);
            }
            catch (Exception ex)
            {
            }
        }

        struct Rango
        {
            public int Inicio { get; set; }
            public int Final { get; set; }
        }

        public void SincronizarActas()
        {
            int rangoInicio = 1;
            int rangoFinal = 20;
            var rangos = new List<Rango>();

            while (rangoFinal <= 18200)
            {
                var rango = new Rango()
                {
                    Inicio = rangoInicio,
                    Final = rangoFinal
                };
                rangos.Add(rango);

                rangoInicio += 20;
                rangoFinal += 20;
            }

            Parallel.ForEach(rangos, el =>
            {
                RecargarActas(el.Inicio, el.Final);
            });
        }
    }
}
