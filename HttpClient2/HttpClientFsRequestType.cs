﻿namespace Helpers.Web
{
    public enum HttpClient2RequestType
    {
        Get,
        PostJson,
        PostTextPlain,
        PostFormData,
        Put,
        Delete
    }
}