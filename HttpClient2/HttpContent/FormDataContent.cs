﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Helpers.Web.HttpContent
{

    public static class FormDataContentHelpers
    {

        public static System.Net.Http.HttpContent ModelToStringContent(object data)
        {
            return new StringContent(
                (data == null ? "" : JsonConvert.SerializeObject(data))
                , Encoding.Unicode, "application/json");
        }

    }

    public class FormDataContent
    {

        public System.Net.Http.HttpContent Contenido { get; set; }
        public string Alias { get; set; }

    }

    public class FormDataContentWithFile : FormDataContent
    {

        public string NombreDelArchivo { get; set; }

    }

}