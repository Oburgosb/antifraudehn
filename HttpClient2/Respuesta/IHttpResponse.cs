﻿using System.Net;

namespace Helpers.Web.Respuesta
{
    public interface IHttpResponse
    {
        HttpStatusCode StatusCode { get; set; }
        string Content { get; set; }
        ResponseType RespuestaTipo { get; set; }
        bool Ok { get; }
    }

    public interface IHttpResponse<TEntidad> : IHttpResponse
        where TEntidad : class
    {
        TEntidad Data { get; set; }
    }
}