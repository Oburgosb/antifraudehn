﻿using System.Net;

namespace Helpers.Web.Respuesta
{
    public class HttpResponse : IHttpResponse
    {
        private string _content;
        public string Content
        {
            get => _content;
            set
            {
                _content = value;
                if (_content.Contains("{\"message\":\""))
                    _content = _content.Replace("{\"message\":\"", "").Replace("\"}", "");
            }
        }
        
        public HttpStatusCode StatusCode { get; set; }

        public ResponseType RespuestaTipo { get; set; }

        public HttpResponse()
        {
            RespuestaTipo = ResponseType.Ok;
        }

        public bool Ok => RespuestaTipo == ResponseType.Ok;
    }

    public class HttpResponse<TEntidad> : HttpResponse, IHttpResponse<TEntidad> where TEntidad : class
    {
        public TEntidad Data { get; set; }
        public HttpResponse() 
        {
            Data = null;
        }
    }
}