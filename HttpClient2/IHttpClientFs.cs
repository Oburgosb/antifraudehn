﻿using System.Threading.Tasks;
using Helpers.Web.Respuesta;

namespace Helpers.Web
{
    public interface IHttpClient2
    {
        string UrlBase { get; set; }
        bool UsarUrlBase { get; set; }

        void AddBearerAuthentication(string value);
        IHttpResponse<TData> Get<TData>(string url = "") where TData : class;
        Task<IHttpResponse<TData>> GetAsync<TData>(string url = "") where TData : class;
        IHttpResponse<TEntidad> MakeRequest<TEntidad>(HttpClient2RequestType tipo, object data = null, string url = "") where TEntidad : class;
        Task<IHttpResponse<TEntidad>> MakeRequestAsync<TEntidad>(HttpClient2RequestType tipo, object data = null, string url = "") where TEntidad : class;
        IHttpResponse Post(string url = "", object data = null);
        IHttpResponse<TData> Post<TData>(string url = "", object data = null) where TData : class;
        Task<IHttpResponse> PostAsync(string url = "", object data = null);
        Task<IHttpResponse<TData>> PostAsync<TData>(string url = "", object data = null) where TData : class;
    }
}