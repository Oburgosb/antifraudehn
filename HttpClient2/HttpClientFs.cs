﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using Helpers.Web.HttpContent;
using Newtonsoft.Json;
using Helpers.Web.Respuesta;

namespace Helpers.Web
{

    public class HttpClient2 : IHttpClient2
    {

        public bool UsarUrlBase { get; set; }
        private string _urlBase;
        public string UrlBase
        {
            get => _urlBase;
            set
            {
                _urlBase = value;
                UsarUrlBase = !string.IsNullOrWhiteSpace(value);
            }
        }

        private AuthenticationHeaderValue _headerAuthentication = null;

        public void AddBearerAuthentication(string value)
        {
            _headerAuthentication = new AuthenticationHeaderValue("Bearer", value);
        }

        private HttpClient HttpClientInstance()
        {
            var clienteHttp = new HttpClient();
            clienteHttp.DefaultRequestHeaders.Accept.Clear();

            if (_headerAuthentication != null)
                clienteHttp.DefaultRequestHeaders.Authorization = _headerAuthentication;

            return clienteHttp;
        }

        public HttpClient2()
        {
        }

        public HttpClient2(string urlBase)
        {
            UrlBase = urlBase;
            UsarUrlBase = true;
        }

        #region Post
        public IHttpResponse Post(string url = "", object data = null)
        {
            return MakeRequest<object>(HttpClient2RequestType.PostJson, data, url);
        }

        public IHttpResponse<TData> Post<TData>(string url = "", object data = null) where TData : class
        {
            return MakeRequest<TData>(HttpClient2RequestType.PostJson, data, url);
        }

        public async Task<IHttpResponse> PostAsync(string url = "", object data = null)
        {
            return await MakeRequestAsync<object>(HttpClient2RequestType.PostJson, data, url);
        }

        public async Task<IHttpResponse<TData>> PostAsync<TData>(string url = "", object data = null) where TData : class
        {
            return await MakeRequestAsync<TData>(HttpClient2RequestType.PostJson, data, url);
        }

        #endregion

        #region Get

        public async Task<IHttpResponse<TData>> GetAsync<TData>(string url = "") where TData : class
        {
            return await MakeRequestAsync<TData>(HttpClient2RequestType.Get, url: url);
        }

        public IHttpResponse<TData> Get<TData>(string url = "") where TData : class
        {
            return MakeRequestAsync<TData>(HttpClient2RequestType.Get, url: url).Result;
        }

        #endregion

        #region  Http

        public IHttpResponse<TEntidad> MakeRequest<TEntidad>(HttpClient2RequestType tipo,
            object data = null, string url = "") where TEntidad : class
        {
            return MakeRequestAsync<TEntidad>(tipo, data, url).Result;
        }

        public async Task<IHttpResponse<TEntidad>> MakeRequestAsync<TEntidad>(HttpClient2RequestType tipo,
            object data = null, string url = "") where TEntidad : class
        {
            if (UsarUrlBase)
            {
                url = System.IO.Path.Combine(UrlBase, url).Replace(@"\", "/");
            }

            try
            {
                using (HttpClient client = HttpClientInstance())
                {
                    System.Net.Http.HttpContent content;

                    // Add Content
                    switch (tipo)
                    {
                        case HttpClient2RequestType.PostTextPlain:
                            content = new StringContent(
                                data.ToString()
                                , Encoding.Unicode, "text/plain");
                            break;
                        case HttpClient2RequestType.PostJson:
                        case HttpClient2RequestType.Get:
                        case HttpClient2RequestType.Delete:
                        case HttpClient2RequestType.Put:
                            client.DefaultRequestHeaders.Accept.Add(new
                                MediaTypeWithQualityHeaderValue("application/json"));
                            content = new StringContent(
                                (data == null ? "" : JsonConvert.SerializeObject(data))
                                , Encoding.Unicode, "application/json");
                            break;
                        default:

                            if (!(data is List<FormDataContent>))
                                throw new InvalidCastException("data debe ser tipo List<FormDataContent>");

                            var formDataList = (List<FormDataContent>)data;

                            var multipartFormDataContent = new MultipartFormDataContent();

                            foreach (var item in formDataList)
                            {
                                if (item is FormDataContentWithFile)
                                {
                                    var itemWithFile = item as FormDataContentWithFile;
                                    multipartFormDataContent.Add(itemWithFile.Contenido,
                                        itemWithFile.Alias, itemWithFile.NombreDelArchivo);
                                }
                                else if (item is FormDataContent)
                                {
                                    multipartFormDataContent.Add(item.Contenido, item.Alias);
                                }
                            }

                            content = multipartFormDataContent;
                            break;
                    }

                    // Do Request
                    HttpResponseMessage res = null;
                    switch (tipo)
                    {
                        case HttpClient2RequestType.PostJson:
                        case HttpClient2RequestType.PostTextPlain:
                            res = await client.PostAsync(url, content);
                            break;
                        case HttpClient2RequestType.Put:
                            res = await client.PutAsync(url, content);
                            break;
                        case HttpClient2RequestType.Delete:
                            res = await client.DeleteAsync(url);
                            break;
                        case HttpClient2RequestType.Get:
                            res = await client.GetAsync(url);
                            break;
                    }

                    return await GetResponse<TEntidad>(res);
                }
            }
            catch (Exception ex)
            {
                return GetResponseException<TEntidad>(ex);
            }
        }

        #endregion

        #region  Response

        private IHttpResponse<TData> GetResponseException<TData>(Exception ex) where TData : class
        {
            var respuesta = new HttpResponse<TData>()
            {
                RespuestaTipo = ResponseType.Excepcion,
                Content = ex.Message
            };
            return respuesta;
        }

        private async Task<IHttpResponse<TData>> GetResponse<TData>(HttpResponseMessage responseMessage) where TData : class
        {
            var respuesta = new HttpResponse<TData>()
            {
                Content = await responseMessage.Content.ReadAsStringAsync(),
                StatusCode = responseMessage.StatusCode
            };
            if (!String.IsNullOrEmpty(respuesta.Content))
            {
                respuesta.Data = DeserializeObject<TData>(respuesta.Content);
            }
            respuesta.RespuestaTipo = (responseMessage.StatusCode == System.Net.HttpStatusCode.OK ?
                ResponseType.Ok : ResponseType.Excepcion);
            return respuesta;
        }
        #endregion

        #region Serializacion
        private TEntidad DeserializeObject<TEntidad>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<TEntidad>(data);
            }
            catch (Exception)
            {
                return default(TEntidad);
            }
        }
        #endregion



    }
}