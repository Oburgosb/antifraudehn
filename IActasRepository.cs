﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TSEAntifraudeHn
{
    public interface IActasRepository
    {

        Task<IEnumerable<Acta>> ObtenerTodas();
        Task<Acta> ObtenerActa(string codigoActa);
        Task<IEnumerable<Acta>> ObtenerPorDepartamento(string departamento, int skip, int take);
        Task RegistrarActa(Acta item);
        Task RegistrarActas(IEnumerable<Acta> items);
        Task RegistrarActasEnLog(IEnumerable<Acta> items);
        Task<bool> EliminarTodo();
        void EliminarActas(List<Acta> actas);
        int Count(string deparamento);
    }
}
