﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace TSEAntifraudeHn.Controllers
{
    [Route("api/Actas")]
    public class ValuesController : Controller
    {
        private readonly IActasRepository _actasRepository;

        public ValuesController(IActasRepository noteRepository)
        {
            _actasRepository = noteRepository;
        }

        [HttpGet, Route("ObtenerTodas")]
        public async Task<IActionResult> ObtenerActas()
        {
            var resultado = await _actasRepository.ObtenerTodas();
            return Ok(resultado.OrderByDescending(x => x.Votos.Sum(y => y.NumVotos)));
        }

        [HttpGet, Route("{deparamento}/{skip}/{take}")]
        public async Task<IActionResult> ObtenerActas(string deparamento, int skip, int take)
        {
            var resultado = await _actasRepository.ObtenerPorDepartamento(deparamento, skip, take);

            resultado = resultado.OrderByDescending(x => x.Votos.Sum(y => y.NumVotos));
            return Ok(new {
                data = resultado,
                totalCount = _actasRepository.Count(deparamento)
            });
        }

        //[HttpPost, Route("RegisterActas")]
        //public IActionResult RegistrarActas([FromBody] List<Acta> actas)
        //{
        //    return Ok(_actasRepository.RegistrarActas(actas));
        //}

    }
}
