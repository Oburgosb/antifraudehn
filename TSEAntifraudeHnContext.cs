﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace TSEAntifraudeHn
{
    public class TSEAntifraudeHnContext
    {

        private readonly IMongoDatabase _database = null;

        public TSEAntifraudeHnContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Acta> Actas
        {
            get
            {
                return _database.GetCollection<Acta>("Actas_Generales");
            }
        }

        public IMongoCollection<ActaSnapshot> ActasLog
        {
            get
            {
                return _database.GetCollection<ActaSnapshot>("Actas_Generales_Log");
            }
        }

    }
}
